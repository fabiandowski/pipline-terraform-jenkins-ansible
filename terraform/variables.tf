#====================#
# vCenter connection #
#====================#

variable "vsphere_user" {
  description = "vSphere user name"
}

variable "vsphere_password" {
  description = "vSphere password"
}

variable "vsphere_vcenter" {
  description = "vCenter server FQDN or IP"
}

variable "vsphere_unverified_ssl" {
  description = "Is the vCenter using a self signed certificate (true/false)"
}

variable "vsphere_datacenter" {
  description = "vSphere datacenter"
}

variable "vsphere_resource_pool" {
  description = "vSphere resource pool"
}

variable "vsphere_host" {
  description = "vSphere resource host"
}

#===========================#
# VMware infrastructure #
#===========================#

variable "vm_user" {
  description = "SSH user for the vSphere virtual machines"
}

variable "vm_password" {
  description = "SSH password for the vSphere virtual machines"
}

variable "vm_distro" {
  description = "Linux distribution of the vSphere virtual machines (ubuntu/centos/debian/rhel)"
}

variable "vm_network" {
  description = "vSphere network"
}

variable "vm_template" {
  description = "Template used to create the vSphere virtual machines (linked clone)"
}

variable "vm_datastore" {
  description = "vSphere datastore"
}

variable "vm_ip" {
  description = "IPs used for the virtual machines"
}

variable "vm_netmask" {
  description = "Netmask used for the virtual machines (example: 24)"
}

variable "vm_nameserver" {
  description = "Nameserver used for the virtual machines"
}

variable "vm_gateway" {
  description = "Gateway for the virtual machine"
}

variable "vm_dns" {
  description = "DNS for the virtual machines"
}

variable "vm_domain" {
  description = "Domain for the virtual machine"
}

variable "vm_cpu" {
  description = "Number of vCPU for virtual machines"
}

variable "vm_ram" {
  description = "Amount of RAM for the virtual machines (example: 2048)"
}

variable "vm_name_prefix" {
  description = "Prefix for the name of the virtual machines and the hostname"
}

#================#
# Redhat account #
#================#

variable "rh_subscription_server" {
  description = "Address of the Redhat subscription server"
  default     = "subscription.rhsm.redhat.com"
}

variable "rh_unverified_ssl" {
  description = "Disable the Redhat subscription server certificate verification"
  default     = "false"
}

variable "rh_username" {
  description = "Username of your Redhat account"
  default     = "none"
}

variable "rh_password" {
  description = "Password of your Redhat account"
  default     = "none"
}
