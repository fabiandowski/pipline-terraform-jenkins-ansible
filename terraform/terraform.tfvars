#===============================================================================
# VMware vSphere configuration
#===============================================================================

# vCenter IP or FQDN #
vsphere_vcenter = "vcenter.cloud.local"

# vSphere username used to deploy the infrastructure #
vsphere_user = "administrator@vsphere.local"

# vSphere password used to deploy the infrastructure #
vsphere_password = "Zsedcx.qwerty.123"

# Skip the verification of the vCenter SSL certificate (true/false) #
vsphere_unverified_ssl = "true"

# vSphere datacenter name where the infrastructure will be deployed #
vsphere_datacenter = "Escuela"

# vSphere resource pool name that will be created to deploy the virtual machines #
vsphere_resource_pool = "FMENDIETA"

# Enable anti-affinity between the Kubernetes master virtual machines. This feature require a vSphere enterprise plus license #
vsphere_enable_anti_affinity = "false"

#===============================================================================
# Global virtual machines parameters
#===============================================================================

# Username used to SSH to the virtual machines #
vm_user = "root"

# Password used to SSH to the virtual machines #
vm_password = "3ficient3.."

# The linux distribution used by the virtual machines (ubuntu/debian/centos/rhel) #
vm_distro = ""

# The IP template the virtual machine
vsphere_host = "172.19.2.104"

# The datastore name used to store the files of the virtual machines #
vm_datastore = "DS_104"

# The vSphere network name used by the virtual machines #
vm_network = "VLAN_102"

# The DNS server used by the virtual machines #
vm_dns = "[8.8.8.8][8.8.4.4]"

# The vSphere template the virtual machine are based on #
vm_template = "template-kubernetes"

vm_nameserver = "10.10.10.100"
#===============================================================================
# Master node virtual machines parameters
#===============================================================================

# The name allocated to the virtual machines #
vm_name_prefix = "test8"

# The domain name used by the virtual machines #
vm_domain = "test.internal"

# The number of vCPU allocated to the virtual machines #
vm_cpu = "2"

# The amount of RAM allocated to the virtual machines #
vm_ram = "4096"

# The IP addresses of the master machines. You need to define 1 IP #
vm_ip = "172.19.2.94"

# The netmask used to configure the network cards of the virtual machines (example: 24)#
vm_netmask = "24"

# The network gateway used by the virtual machines #
vm_gateway = "172.19.2.1"


#===============================================================================
# Redhat subscription parameters
#===============================================================================

# If you use RHEL 7 as a base distro, you need to specify your subscription account #
rh_subscription_server = "subscription.rhsm.redhat.com"
rh_unverified_ssl = "false"
rh_username = ""
rh_password = ""
