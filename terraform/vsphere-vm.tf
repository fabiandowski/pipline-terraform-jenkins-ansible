#===============================================================================
# vSphere Provider
#===============================================================================

provider "vsphere" {
  vsphere_server       = "${var.vsphere_vcenter}"
  user                 = "${var.vsphere_user}"
  password             = "${var.vsphere_password}"
  allow_unverified_ssl = "${var.vsphere_unverified_ssl}"
}

#===============================================================================
# vSphere Data
#===============================================================================

data "vsphere_datacenter" "dc" {
  name = "${var.vsphere_datacenter}"
}

data "vsphere_datastore" "datastore" {
  name          = "${var.vm_datastore}"
  datacenter_id = "${data.vsphere_datacenter.dc.id}"
}

data "vsphere_network" "network" {
  name          = "${var.vm_network}"
  datacenter_id = "${data.vsphere_datacenter.dc.id}"
}

data "vsphere_virtual_machine" "template" {
  name          = "${var.vm_template}"
  datacenter_id = "${data.vsphere_datacenter.dc.id}"
}

data "vsphere_resource_pool" "resource_pool" {
  name          = "${var.vsphere_resource_pool}"
  datacenter_id = "${data.vsphere_datacenter.dc.id}"
}

data "vsphere_host" "host" {
  name          = "${var.vsphere_host}"
  datacenter_id = "${data.vsphere_datacenter.dc.id}"
}
#===============================================================================
# vSphere Resources
#===============================================================================

# Create the VMs #
resource "vsphere_virtual_machine" "test9"{
  name             = "${var.vm_name_prefix}"
  num_cpus         = "${var.vm_cpu}"
  memory           = "${var.vm_ram}"
  datastore_id     = "${data.vsphere_datastore.datastore.id}"
  host_system_id   = "${data.vsphere_host.host.id}" 
  resource_pool_id = "${data.vsphere_resource_pool.resource_pool.id}"
  guest_id         = "${data.vsphere_virtual_machine.template.guest_id}"
  scsi_type        = "${data.vsphere_virtual_machine.template.scsi_type}"
# Set network parameters
  network_interface {
    network_id   = "${data.vsphere_network.network.id}"
  }

# Use a predifined vmware template has main disk
  disk {
    label            = "${var.vm_name_prefix}.vmdk"
    size             = "${data.vsphere_virtual_machine.template.disks.0.size}"
    thin_provisioned = "${data.vsphere_virtual_machine.template.disks.0.thin_provisioned}"
  }

  clone {
    template_uuid = "${data.vsphere_virtual_machine.template.id}"

    customize {
      timeout = "20"

      linux_options {
        host_name = "${var.vm_name_prefix}"
        domain    = "${var.vm_domain}"
      }

      network_interface {
        ipv4_address = "${var.vm_ip}"
        ipv4_netmask = "${var.vm_netmask}"
      }

      ipv4_gateway    = "${var.vm_gateway}"
    }
  }

  provisioner "remote-exec" {
    inline = [
      "echo 'nameserver ${var.vm_nameserver}' > /etc/resolv.conf",
      "apt update -y",
      "apt-get install net-tools -y",
      "apt-get install ansible -y",
    ]

    connection {     
      type	   = "ssh"
      host 	   = "${var.vm_ip}"
      user 	   = "${var.vm_user}"
      password     = "${var.vm_password}"  
    }
  }
}
